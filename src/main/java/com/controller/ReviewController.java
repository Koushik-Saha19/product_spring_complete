package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dxc.pms.model.Product;
import com.dxc.pms.model.Review;
import com.dxc.pms.service.ReviewService;

@RestController
@RequestMapping("review")
@CrossOrigin(origins= {"http://localhost:3000","http://localhost:4200"})
public class ReviewController {

	@Autowired
	ReviewService reviewService;
	Product product;
	//To fetch all Reviews of a particular product
		@GetMapping("/{productId}")
		public ResponseEntity<List<Review>> getAllReview(@PathVariable("productId")int productId) {
			System.out.println("Fetching all reviews" );
			System.out.println(productId);
			List<Review> allReview = reviewService.getAllReview(productId);
			return new ResponseEntity<List<Review>>(allReview,HttpStatus.OK);
		}
		
		
		
		@PostMapping()
		public boolean saveProduct(@RequestBody Review review) {
			System.out.println("saving review");
     		reviewService.addReview(review);
     		
     		return true;
			
//			if(productService.isProductExist(product.getProductId())) {
//				
//				return new ResponseEntity<Product>(product,HttpStatus.CONFLICT);
//			}
//			else {
//				productService.addProduct(product);
//				return new ResponseEntity<Product>(product,HttpStatus.CREATED);
//			}
			
			
		}
	
}
