package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.dxc.pms.model.Product;
import com.dxc.pms.service.ProductService;

@RestController
@RequestMapping("product")
@CrossOrigin(origins= {"http://localhost:3000","http://localhost:4200"})
public class ProductController {

	@Autowired
	ProductService productService;
	

	//To fetch all products
	@GetMapping
	public ResponseEntity<List<Product>> getAllProduct() {
		System.out.println("Fetching all products" );
		List<Product> allProducts = productService.getProducts();
		return new ResponseEntity<List<Product>>(allProducts,HttpStatus.OK);
	}
	

	//Getting a single product
	@GetMapping("/{productId}")
	public ResponseEntity<Product> getProduct(@PathVariable("productId")int productId) {
		System.out.println("Get product 1 called " + productId);
//		return productService.getProduct(productId);
		if(productService.isProductExist(productId)) {
			Product product = productService.getProduct(productId);
			return new ResponseEntity<Product>(product,HttpStatus.OK);
		}
		else {
			return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
		}
	}
	
	//to delete product
	@DeleteMapping("/{productId}")
	public ResponseEntity<Product> deleteProduct(@PathVariable("productId")int productId) {
		System.out.println("delete product called " );
	
		if(productService.isProductExist(productId)) {
			productService.deleteProduct(productId);
			Product product = productService.getProduct(productId);
			return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
		}
		else {
			return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
		}
	}
	
	
	
	
//	@RequestMapping("/getProduct/{productId}/orders/{oId}")
//	public Product getProduct(@PathVariable("productId")int productId,@PathVariable("oId")int oId) {
//		System.out.println("Get product 2 is called with productId and orderId " + productId + "and " + oId);
//		return productService.getProduct(productId); 
//	}
//	
//	
	@PostMapping()
	public ResponseEntity<Product> saveProduct(@RequestBody Product product) {
		System.out.println("saving product");
//		return productService.addProduct(product);
		
		if(productService.isProductExist(product.getProductId())) {
			
			return new ResponseEntity<Product>(product,HttpStatus.CONFLICT);
		}
		else {
			productService.addProduct(product);
			return new ResponseEntity<Product>(product,HttpStatus.CREATED);
		}
		
		
	}
	@PutMapping()
	public ResponseEntity<Product> updateProduct(@RequestBody Product product) {
		System.out.println("update a product called");
		
		if(productService.isProductExist(product.getProductId())) {
			productService.updateProduct(product);
			return new ResponseEntity<Product>(product,HttpStatus.OK);
		}
		else {
			return new ResponseEntity<Product>(product,HttpStatus.NOT_FOUND);
		}
			
//		return productService.updateProduct(product);	
	}
	
	@GetMapping("/search/{productName}")
	public ResponseEntity<List<Product>> searchProductByName(@PathVariable("productName")String productName){
		System.out.println("searchProductByName is called");
		List<Product> allProducts = productService.searchProductByName(productName);
		return new ResponseEntity<List<Product>>(allProducts,HttpStatus.OK);
		
				
	}
	
	
}
	
	
	
	

