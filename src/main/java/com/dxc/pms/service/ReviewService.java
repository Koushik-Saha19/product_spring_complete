package com.dxc.pms.service;

import java.util.List;

import com.dxc.pms.model.Review;

public interface ReviewService {

	public List<Review> getAllReview(int productId);
	public boolean addReview(Review review);
	
}
