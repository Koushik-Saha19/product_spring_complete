package com.dxc.pms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dxc.pms.dao.ReviewDAO;
import com.dxc.pms.model.Product;
import com.dxc.pms.model.Review;

@Service
public class ReviewServiceImpl implements ReviewService {

	@Autowired
	ReviewDAO reviewDAO; 
	
	Product product;
	@Override
	public List<Review> getAllReview(int productId) {
		System.out.println(productId);
		return reviewDAO.getAllReview(productId);
		
	}
	@Override
	public boolean addReview(Review review) {
		return reviewDAO.addReview(review);
	}

	
}
