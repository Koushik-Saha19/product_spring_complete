package com.dxc.pms.dao;

import java.util.List;

import com.dxc.pms.model.Product;
import com.dxc.pms.model.Review;

public interface ReviewDAO {

	
	public List<Review> getAllReview(int productId);
	public boolean addReview(Review review);
	
	
}
