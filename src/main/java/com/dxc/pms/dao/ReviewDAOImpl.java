package com.dxc.pms.dao;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.dxc.pms.model.Product;
import com.dxc.pms.model.Review;

@Repository
public class ReviewDAOImpl implements ReviewDAO{

	@Autowired
	ProductDAO productDAO;
	@Autowired
	MongoTemplate mongoTemplate;
	
	Product product = new Product();
	Review review = new Review();
	@Override
	public List<Review> getAllReview(int productId) {
		System.out.println(productId);
		
		System.out.println(productId);
		Query query = new Query(Criteria.where("productId").is(productId));
		
		List<Review> allReview = mongoTemplate.find(query, Review.class, "review");
		System.out.println(allReview);
		
		//return mongoTemplate.findAll(Review.class);
		return allReview;
	}

	@Override
	public boolean addReview(Review review) {

		product = productDAO.getProduct(review.getProductId());
		
		
		
		product.setReviews(review);
		
		System.out.println("Inside DAO :" + review);
        System.out.println("Inside DAO :" + product);
		
        
        
        
		mongoTemplate.save(product, "product");
		return true;
	}

	

	
}
