package com.dxc.pms.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
@Document
public class Review {

	@Id
	private int reviewId;
	private String review;
	private String reviewRating;
	private int productId;
	
	public Review() {
		// TODO Auto-generated constructor stub
	}

	public Review(int reviewId, String review, String reviewRating, int productId) {
		super();
		this.reviewId = reviewId;
		this.review = review;
		this.reviewRating = reviewRating;
		this.productId = productId;
	}

	public int getReviewId() {
		return reviewId;
	}

	public void setReviewId(int reviewId) {
		this.reviewId = reviewId;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	public String getReviewRating() {
		return reviewRating;
	}

	public void setReviewRating(String reviewRating) {
		this.reviewRating = reviewRating;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "Review [reviewId=" + reviewId + ", review=" + review + ", reviewRating=" + reviewRating + ", productId="
				+ productId + "]";
	}


	
	
}
